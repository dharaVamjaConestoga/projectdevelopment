## To use this project,

1. Clone project
2. Open with Visual Studio Code
3. open terminal and run npm start
4. It will open the browser and user can use the website!

Removed text to revert things!

**GNU GPL v3**

License: GPL v3

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-green.svg)](https://www.gnu.org/licenses/gpl-3.0)



**MIT**

The MIT License

License: MIT

[![License: MIT](https://img.shields.io/badge/License-MIT-red.svg)](https://opensource.org/licenses/MIT)

**Note**
This codebase is for learning purpose. Anyone can learn, update and use this code as per their requirement. Since I do not have anything to protect, I would like to go with this license.