/**
 * Name: Dhara Vamja
 * Program: Midterm Set 2 - John’s Printshop 
 * Date Created: 07/07/2021
 */


//#region - Variables
/**
 * Regex Validation List
 */
var regexValidations = new Array();
regexValidations['empty_string'] = /^$/;
regexValidations['quantity'] = /^[0-9][0-9]*$/
regexValidations['t_postcode'] = /^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]\d[ABCEGHJ-NPRSTV-Z]\d$/i

/**
 * Error Messages List
 */
var errorMessages = new Array();
errorMessages['empty_string'] = "This field can not be empty";
errorMessages['t_name'] = "A valid name is required.";
errorMessages['t_postcode'] = "A valid postal code is required.";
errorMessages['quantity'] = "A valid quantity count is required.";

const GST_RATE = 0.13;
// Available Products
var products = new Array();

// Cart Products
var cartProducts = new Array();
var productIds = ['product1', 'product2', 'product3'];
var isError = false;
//#endregion

//#region - Products list manipulation

/**
 * Add available products data to products array
 */
function addAvailableProducts() {
    products = [];
    for (productIdIndex in productIds) {
        var productId = productIds[productIdIndex]
        let product = getProductObjectAsPerDisplayData(productId);
        products.push(product);
    }
}

/**
 * populate product object as per display data
 * @param {unique id as per product} productId 
 * @returns Product object
 */
function getProductObjectAsPerDisplayData(productId) {
    var displayName = document.getElementById("t_name_" + productId).innerText;
    var productQuantity = document.getElementById("t_quantity_" + productId).value;
    let product = {
        "id": productId,
        "name": displayName,
        "quantity": productQuantity,
        "price": document.getElementById("t_price_" + productId).innerText,
    };
    return product;
}

/**
 * Empty pre-stored data of cart 
 */
function emptyProductLists() {
    cartProducts = [];
    products = [];
}

/**
 * @returns min valid quantity present flag
 */
function isMinimumQuantityAdded() {
    products.forEach(product => {
        if (!isNaN(product.quantity) && product.quantity > 0) {
            return true;
        }
    });
    return false;
}
//#endregion

//#region - Checkout proccess
/**
 * On checkout button clicked - procced to validations and receipt calculation
 * @param {event} event 
 */
function onCheckoutDetails(event) {
    emptyProductLists();
    event.preventDefault();
    isError = false;
    addAvailableProducts();
    validateInputs(event);
    document.getElementById('min_quantity_error').innerHTML = "";
    if (cartProducts.length == 0) {
        document.getElementById('min_quantity_error').innerHTML = "Atleast 1 product is required to procced further!";
    } else if (!isError) {
        showPaymentReceipt();
        //the customer information collected
        document.getElementById('receipt_name').innerHTML = 'Name: ' + event.target.t_name.value;
        document.getElementById('receipt_address_details').innerHTML = 'Postal Code: ' + event.target.t_postcode.value;
        document.getElementById('receipt_total_product_count').innerHTML = 'Total Product Count: ' + cartProducts.length;
    }
}

/**
 * Display final calculated total as required - Hide products div and shows checkout container and populate information
 */
function showPaymentReceipt() {
    document.getElementById("dv_main_container").style.display = 'none';
    document.getElementById("dv_checkout").style.display = 'block';

    var total = 0.0;

    var receipt = "";
    for (let productIndex in cartProducts) {

        var product = cartProducts[productIndex];
        var quantity = parseInt(product.quantity);
        var productPrice = parseFloat(product.price.replace("$", ""));
        var productTotal = parseFloat((quantity * productPrice).toFixed(2));
        addProductInfoToReceipt(product.name, quantity, productPrice, productTotal);
        total += productTotal;
    }

    // Sub total before tax
    addSubTotalWithoutTaxToReceipt();

    //Total tax
    var applicableTax = total * GST_RATE;
    addApplicableTaxInfoToReceipt();

    //the sales total : Total payable with tax
    var totalPayable = total + applicableTax;
    addTotalAmountInfoToReceipt();

    // Show Receipt
    document.getElementById('receipt_body').innerHTML = receipt;

    function addProductInfoToReceipt(productText, quantity, productPrice, productTotal) {
        receipt += "<tr><td>" + productText + "</td><td class='tableQuantity'>"
            + quantity + "</td><td class='text-right'>$" + productPrice + "</td>"
            + "</td><td class='text-right'>$" + productTotal + "</td></tr>";
    }

    function addSubTotalWithoutTaxToReceipt() {
        receipt += "<tr><td>Total of services </td><td colspan='3' class='text-right border-top'>$" + total.toFixed(2) + "</td></tr>";
    }

    function addTotalAmountInfoToReceipt() {
        receipt += "<tr><td>Total Payable Amount</td><td colspan='3' class='text-right border-top'>$" + totalPayable.toFixed(2) + "</td></tr>";
    }

    function addApplicableTaxInfoToReceipt() {
        receipt += "<tr><td>Applicable TAX(13%) </td><td colspan='3' class='text-right'>$" + applicableTax.toFixed(2) + "</td></tr>";
    }
}

//#endregion

//#region - Back to Product Listing
/**
 * Go to product listing from checkout invoice
 */
function backToProcucts() {
    document.getElementById("dv_main_container").style.display = 'block';
    document.getElementById("dv_checkout").style.display = 'none';

    let invoiceTable = document.getElementById("receipt_body");
    invoiceTable.innerHTML = "";
}
//#endregion

//#region  Validations - Quantity, Name and Postal Code
/**
 * Validate inputs from form data
 * @param {*} event 
 */
function validateInputs(event) {
    var inputDataForm = event.target;
    for (var inputEntryIndex = 0; inputEntryIndex < inputDataForm.length; inputEntryIndex++) {
        if (inputDataForm.elements[inputEntryIndex].type != 'submit') {
            var name = inputDataForm.elements[inputEntryIndex].name;
            var value = inputDataForm.elements[inputEntryIndex].value.trim();
            inputDataForm[name] = value;
            verifyValidInput(value, name);
        }
    }
}

/**
 * Validate individual input from input array
 * @param {*} inputValue 
 * @param {*} inputName 
 * @returns 
 */
function verifyValidInput(inputValue, inputName) {
    // Product validations - quantity
    if (isProductType()) {
        if (!isEmptyString()) {
            if (isInvalidQuantity()) {
                isError = true;
                document.getElementById(inputName + '_error').innerHTML = errorMessages['quantity'];
                return;
            }
            var product = getProductObjectAsPerDisplayData(inputName);
            if (isAddedToCart(product)) {
                cartProducts.push(product);
            }
        }
        // Clear error
        document.getElementById(inputName + '_error').innerHTML = "";
        return;
    }

    // Checkout Details Validations - name, postal code
    var regex = getAppropriateRegex();
    if (isEmptyString()) {
        isError = true;
        document.getElementById(inputName + '_error').innerHTML = errorMessages['empty_string'];
        return;
    }
    if (regex != undefined) {
        if (isValidationFailed()) {
            isError = true;
            document.getElementById(inputName + '_error').innerHTML = errorMessages[inputName];
            return;
        }
    }

    // Clear error
    document.getElementById(inputName + '_error').innerHTML = "";

    function isProductType() {
        return productIds.indexOf(inputName) != -1;
    }

    function isValidationFailed() {
        return !regex.test(inputValue);
    }

    function isEmptyString() {
        return regexValidations['empty_string'].test(inputValue);
    }

    function getAppropriateRegex() {
        return regexValidations[inputName];
    }

    function isInvalidQuantity() {
        return !regexValidations['quantity'].test(inputValue);
    }
}

function isAddedToCart(product) {
    return product.quantity > 0;
}

//#endregion

//#region  Event Listners for Show/hide description
/**
 * Click event for Product 1 expand - Show more text - hide label itself and show description text
 */
$(document).on('click', ' #product1_expand', function (e) {
    e.preventDefault();
    $('#product1_expand_text').show();
    $(this).hide();
});

/**
 * Click event for Product 1 collapse - Hide Details text - hide label itself along with description text and make show more visible
 */
$(document).on('click', ' #product1_collapse', function (e) {
    e.preventDefault();
    $('#product1_expand_text').hide();
    $('#product1_expand').show();
});

/**
 * Click event for Product 2 expand - Show more text - hide label itself and show description text
 */
$(document).on('click', ' #product2_expand', function (e) {
    e.preventDefault();
    $('#product2_expand_text').show();
    $(this).hide();
});

/**
 * Click event for Product 2 collapse - Hide Details text - hide label itself along with description text and make show more visible
 */
$(document).on('click', ' #product2_collapse', function (e) {
    e.preventDefault();
    $('#product2_expand_text').hide();
    $('#product2_expand').show();
});

/**
 * Click event for Product 3 expand - Show more text - hide label itself and show description text
 */
$(document).on('click', ' #product3_expand', function (e) {
    e.preventDefault();
    $('#product3_expand_text').show();
    $(this).hide();
});

/**
 * Click event for Product 3 collapse - Hide Details text - hide label itself along with description text and make show more visible
 */
$(document).on('click', ' #product3_collapse', function (e) {
    e.preventDefault();
    $('#product3_expand_text').hide();
    $('#product3_expand').show();
});

//#endregion